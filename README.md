- Create directory simple-webserver
mkdir ~/simple-webserver

cd ~/simple-webserver

- Deploy simple webserver (webserver.yaml)
kubectl create -f webserver.yaml

kubectl get deployments

kubectl get pods

- Create Service ServiceType NodePort (webserver-svc.yaml)
kubectl create -f webserver-svc.yaml

kubectl get svc
